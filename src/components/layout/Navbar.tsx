import React from 'react';

const Navbar = () => {
  return (
    <nav className="h-[82px] bg-primary text-secondary-foreground z-10 relative">
      <div className="max-w-5xl h-full flex justify-between items-center mx-auto">
        <h2 className="text-secondary font-bold text-2xl mx-6">CON3X</h2>
      </div>
    </nav>
  );
};

export default Navbar;
