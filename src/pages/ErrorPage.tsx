import { Button } from '../components/ui/button';

const ErrorPage = () => {
  return (
    <div style={{ height: '80vh' }} className="mx-auto max-w-lg flex flex-col gap-4 items-center justify-center">
      <h1 className="text-center text-2xl sm:text-4xl font-semibold text-primary">ERROR</h1>
      <p>Something Went Wrong Please Try Again</p>
      <Button>TRY AGAIN</Button>
    </div>
  );
};

export default ErrorPage;
